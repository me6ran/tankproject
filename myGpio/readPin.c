#include <stdlib.h>
#include <stdio.h> //printf fprintf
#include <wiringPi.h>
#include <stdbool.h>

// const int gpio_5 = 21;
const int gpio_6 = 22;


void initGpio(){
    wiringPiSetup();
    // pinMode(gpio_5,INPUT);
    pinMode(gpio_6,INPUT);
    // pullUpDnControl(gpio_5,PUD_UP);
    pullUpDnControl(gpio_6,PUD_UP);

}

int main(void){
    initGpio();  
    // fprintf(stdout,"%d:%d",(digitalRead(gpio_5) ? 0:1),(digitalRead(gpio_6)? 0:1));
    fprintf(stdout,"%d",digitalRead(gpio_6));
    return EXIT_SUCCESS;
}