from sendAlarm import EmailSending
from sys import argv

value = None

try:
    value = argv[1]
except:
    pass


body = "Tank Level Has Been Restored, Current Level: %s" % (value)
subject = "Restored Tank Level Alarm"
myemail = EmailSending(subject,body)
myemail.sendEmail()
del myemail
