from sendAlarm import EmailSending
from sys import argv

value = None

try:
    value = argv[1]
except:
    pass


# print(hiTemp)
body = "Tank level goes lower than %s(cm)" % (value)
subject = "Low Tank Level Alarm"
myemail = EmailSending(subject,body)
myemail.sendEmail()
del myemail
