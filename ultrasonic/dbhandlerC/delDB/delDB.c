#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <syslog.h>
#define Name 1


void logging(char *, int);

static int callback(void *data, int argc, char **argv, char **clumName){
    printf("inside callBack\n");
    for(int i=0;i<argc;i++){
        printf("%s",argv[i] ? argv[i]:"NULL");
    }
    return 0;
}


int main(int argc, char **argv){
    if(argc!=2){
        fprintf(stderr,"Not enough Argument\n");
        return 0;
    }
    else{
        int ret;
        char sql[70];
        char *errmsg;
        sqlite3 *db;
        char *dbAddrss = "/home/pi/dataBase/tank.db";
        const char *data = "CallBack";

        ret = sqlite3_open(dbAddrss,&db);
        
        if(ret){
            fprintf(stderr,"Unable to open database: %s\n",sqlite3_errmsg(db));
            return 0;
        }
        else{
            sprintf(sql, "DELETE FROM emailList WHERE Name='%s';",argv[Name]);
            // printf("Sql: %s",sql);

            ret = sqlite3_exec(db,sql,callback,(void*)data,&errmsg);
            if(ret != SQLITE_OK){
                fprintf(stderr,"SQL error: %s\n",errmsg);
                sqlite3_free(errmsg);
                return 0;
            }
            else{
                fprintf(stdout,"The user: %s has been deleted from Email list\n",argv[Name]);
                logging(argv[Name],LOG_INFO); 
            }
        }
        sqlite3_close(db);
        return 0;

    }
}

void logging(char *logMsg, int LOGLevel){

	openlog("Tank ", LOG_CONS | LOG_PID, LOG_USER);
	
	syslog(LOGLevel,"The user: %s has been deleted from Email list.",logMsg);
	closelog();

	}
