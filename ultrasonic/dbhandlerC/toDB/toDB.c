#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <string.h>
#include <time.h>

void usage(void);

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
   
	/*
	* Arguments:
	*
	*   unused - Ignored in this case, see the documentation for sqlite3_exec
	*    count - The number of columns in the result set
	*     data - The row's data
	*  columns - The column names
	*/
   int i;
   for(i = 0; i<argc; i++) {
      printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
   }
   printf("\n");
   return 0;
}

int main(int argc, char ** argv){
	if(argc!=2){
	usage();
	}
	char *dbAddrss = "/home/pi/dataBase/tank.db";
	sqlite3 *db;
	char *errMsg=0;
	int rc;
	char insSQL[200]={0};
	int value=0;
	value = atoi(argv[1]);

	if((sqlite3_open(dbAddrss,&db))!=0){
		fprintf(stderr,"Cannot open DB: %s\n",sqlite3_errmsg(db));
	}

	else{
		time_t myepch;
		struct tm *loct;
		myepch = time(NULL);
		loct = localtime(&myepch);
		char buffTime[100];
		strftime(buffTime,100,"%b-%d-%g %H:%M",loct);
		sprintf(insSQL,"INSERT INTO level (epoch,datetime,value) VALUES (%ld,'%s',%d)",myepch,buffTime,value);
		
	
		// printf("%s\n\n",insSQL);
		rc = sqlite3_exec(db,insSQL,callback,0,&errMsg);
		if(rc!= SQLITE_OK){
			fprintf(stderr,"SQL error: %s\n", errMsg);
			sqlite3_free(errMsg);
			sqlite3_close(db);
			exit(0);
		}

	// else{
	// 	fprintf(stdout,"Data has been stored successfully\n");
	// }

	}
	sqlite3_close(db);

	return 0;
}

void usage(void){
	printf("./toDB [level value]\n");
	exit(0);
}
