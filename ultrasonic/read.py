from time import time
from serial import Serial

serialDevice = "/dev/ttyUSB0" # default for RaspberryPi


def read(dly=None):
    ser = Serial(serialDevice, 9600, 8, 'N', 1, timeout=1)
    try:
        b=0
        while(b<56):
            b=ser.inWaiting()
        rawD = ser.read(b)
        ser.close()
        # print(rawD)
        strData = rawD.decode()
        # print(rawD)
        values = makeValue(strData)
        return most_common(values)
    except Exception as e:
        # print(e)
        return -1

def extract(rawData):
    for data in rawData.split("\r"):
        if(len(data)==4):
            return(int(data[1:]))
            break

def makeValue(decodData):
    myvalues=[]
    for ch in decodData.split("\r"):
        try:
            myvalues.append(int(ch[1:]))
        except:
            pass
    return myvalues

def most_common(myList):
    try:
        return max(set(myList), key=myList.count)
    except:
        exit(0)

if __name__ == '__main__':
    dist=read()
    # print(dist)
    if(dist<25):
        print(25)
    else:
        print(dist)
