
# coding: utf-8

# In[1]:


from makeChartPic import Plotting
myplot = Plotting(days=2)
myplot.makePlot()
print(myplot.fileAddress)


# In[9]:


aveValues = sum(myplot.ValueList) / len(myplot.ValueList)
maxValue = max(myplot.ValueList)
minValue = min(myplot.ValueList)
print("Ave: %d\nMax: %d\nMin: %d" % (aveValues,maxValue,minValue) )


# In[10]:


def most_common(lst):
    return max(set(lst), key=lst.count)


# In[11]:


most_common(myplot.ValueList)


# In[105]:


import requests
import json
import datetime


# In[123]:


daysBefore = datetime.datetime.today() - datetime.timedelta(days=10)
dateFrom=daysBefore.strftime("%Y-%m-%d")
today=datetime.datetime.now().strftime("%Y-%m-%d")


# In[122]:


daysBefore.strftime("%Y-%m-%d")


# In[126]:


uri = "https://api.football-data.org/v2/"
headers = {'X-Auth-Token': '8b3d3cac94294cdeae745f087cc1bd59'}
# res = requests.get(uri+"competitions?plan=TIER_ONE",headers=headers)
res = requests.get(uri+"competitions/PL/matches?status=FINISHED&dateFrom=%s&dateTo=%s" %(dateFrom,today),headers=headers)


# In[135]:


for match in res.json()['matches']:
    print(match['awayTeam']['name'],match['score']['fullTime']['awayTeam'],match['homeTeam']['name'],match['score']['fullTime']['homeTeam'])


# In[128]:


res.json()['matches']


# SCHEDULED | LIVE | IN_PLAY | PAUSED | FINISHED | POSTPONED | SUSPENDED | CANCELED }","errorCode"

# In[104]:


uri = "https://api.football-data.org/v2/"
headers = {'X-Auth-Token': '8b3d3cac94294cdeae745f087cc1bd59'}
# res = requests.get(uri+"competitions?plan=TIER_ONE",headers=headers)
res = requests.get(uri+"competitions/PL/matches?status=FINISHED&limit=5",headers=headers)
print(len(res.json())

# for match in res.json()['matches']:
#     print("%s vs %s" % (match['homeTeam']['name'],match['awayTeam']['name']))


# In[78]:


res.json()['matches']


# In[26]:




