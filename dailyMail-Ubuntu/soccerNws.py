import requests
import json
import datetime

uri = "https://api.football-data.org/v2/"
headers = {'X-Auth-Token': '45f0ea21dd394cdeae787cc1bd5ac949'}
lastNdaysmatches= "competitions/PL/matches?status=FINISHED&dateFrom=%s&dateTo=%s"


class GetSoccerNews:
    def __init__(self,days=10):
        self.daysBefore= datetime.datetime.today() - datetime.timedelta(days)
        self.dateFrom=self.daysBefore.strftime("%Y-%m-%d")
        self.today = datetime.datetime.now().strftime("%Y-%m-%d")


    def getFinishedMatches(self):
        try:
            self.res = requests.get(uri+ lastNdaysmatches %(self.dateFrom,self.today),headers=headers)
            self.leagueName = self.res.json()['competition']['name']
            self.lastPlays = []
            for match in self.res.json()['matches']:
                self.lastPlays.append("%s-%s   %s-%s" % (match['homeTeam']['name'],match['score']['fullTime']['homeTeam'],\
                match['awayTeam']['name'],match['score']['fullTime']['awayTeam']))
            return self.lastPlays

        except Exception as e:
            print(e)
            return ["No news available"]

    
    def getLeageName(self):
        return self.leagueName
            