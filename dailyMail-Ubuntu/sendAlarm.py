import smtplib
import sqlite3
import base64
import syslog
from requests import get
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart




class EmailSending:

    dbaddres= "./tank.db"
    _name=0
    _email=1
    smtpPath = "smtp.gmail.com"
    port = 587
    devNamePath = "./FName"
    userPassPath = "./userPass"
    rebootEmails = ["hatef.8@gmail.com"]

    def __init__(self,subject,body):
            self.subject = subject
            self.body = body
            self.smtp = self.smtpPath
            self.port = self.port
            self.user,self.passwd = self.getUsrPass(self.userPassPath)
            self.emailList = self.getEmail(self.dbaddres)
            self.FROM = self.getDevName(self.devNamePath)
            self.TO = [ent[self._email] for ent in self.emailList]
            self.msg = """From: %s\nTo: %s\nSubject: %s\n\n%s""" % (self.FROM,",".join(self.TO),self.subject,self.body)
            
            
    @staticmethod
    def getEmail(dbAdress):
        sql = "select * from emailList"
        # print("database: %s" %(dbAdress))
        conn = sqlite3.connect(dbAdress)
        return conn.execute(sql).fetchall()

    @staticmethod
    def getDevName(devNamePath):
        with open(devNamePath,'r') as fd:
            return fd.readline()
    
    @staticmethod
    def getIP():
        publicIP= "https://api.ipify.org"
        try:
            return get(publicIP).text
        except:
            return "Unknow"


    @staticmethod
    def getUsrPass(userPassPath):
        fd = open(userPassPath,'r')
        filecont = fd.readline()
        fd.close()
        return ([base64.b64decode(content).decode() for content in filecont.split(" ")])

    def sendEmail(self):
        try:
            server  = smtplib.SMTP(self.smtp, self.port)
            server.ehlo()
            server.starttls()
            server.login(self.user,self.passwd)
            server.sendmail(self.FROM,self.TO,self.msg)
            server.close()
        except Exception as e :
            syslog.syslog(syslog.LOG_ERR,"Tank - Email Sending Error with Subject: %s, Error: %s"\
             % (self.subject,e))
            # print("Email error: %s"% (e))

    @classmethod
    def sendRebootEmail(cls):
        TO = cls.rebootEmails
        FROM = cls.getDevName(cls.devNamePath).rstrip()
        IP = cls.getIP()
        user,passwd = cls.getUsrPass(cls.userPassPath)
        msg = """From: %s\nTo: %s\nSubject: %s has been rebooted\n\n%s""" % (FROM,",".join(TO),FROM,IP)
        # print(msg)
        try:
            server  = smtplib.SMTP(cls.smtpPath, cls.port)
            server.ehlo()
            server.starttls()
            server.login(user,passwd)
            server.sendmail(FROM,TO,msg)
            server.close()
        except Exception as e:
            # print(e)
            pass


class DailyReport(EmailSending):

    def __init__(self,subject,body,imageFileAddress,imageName):
        super().__init__(subject,body)
        self.img_data = open(imageFileAddress, 'rb').read()
        self.multiMsg = MIMEMultipart()
        self.multiMsg['Subject'] = self.subject
        self.multiMsg['From'] = self.FROM
        self.multiMsg['To'] = ','.join(self.TO)
        self.multiMsg.add_header('Subject:',(self.FROM))
        self.text = MIMEText(self.body)
        self.multiMsg.attach(self.text)
        self.image = MIMEImage(self.img_data,imageName)
        self.image.add_header('Content-Disposition', 'attachment', filename=imageName)
        self.multiMsg.attach(self.image)

        # return self.multiMsg
    def sendReportMail(self):
        try:
            mysmtp = smtplib.SMTP(self.smtpPath,self.port)
            mysmtp.ehlo()
            mysmtp.starttls()
            # mysmtp.ehlo() self.user,self.passwd
            mysmtp.login(self.user,self.passwd)
            mysmtp.sendmail(self.FROM,self.TO,self.multiMsg.as_string())
            # mysmtp.sendmail(self.multiMsg['From'],self.multiMsg['To'],self.multiMsg.as_string())
            mysmtp.quit()
            # self.img_data.close()
        except Exception as e:
            # print(e)
            syslog.syslog(syslog.LOG_ERR,"Tank - Report Email Sending Error wiht Error: %s" % (e))
            exit(0)