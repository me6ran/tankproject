from soccerNws import GetSoccerNews
from makeChartPic import Plotting
from sendAlarm import EmailSending,DailyReport

mynews = GetSoccerNews()
listofPlay = mynews.getFinishedMatches()
leageName = mynews.getLeageName()
sportBody = "%s in last 10 days\n\n%s" % (leageName,"\n".join(listofPlay))

myplot = Plotting(days=1)
myplot.makePlot()
# print("fileName: %s\nFile Address: %s\n" % (myplot.imageName,myplot.fileAddress) )
maxValue = myplot.getMaxValue()
aveValue = myplot.getAveValue()
minValue = myplot.getMinxValue()

listValue= "Average Level:%d\nMaximum Level: %d\nMinimum Level: %d" % (aveValue,maxValue,minValue)

Emailbody= "%s\n\n\n%s\n" %(listValue,sportBody)

# print(Emailbody)

myemail = DailyReport(subject="Daily Report",body=Emailbody,imageFileAddress=myplot.fileAddress,imageName=myplot.imageName)
# print(myemail.text)
myemail.sendReportMail()