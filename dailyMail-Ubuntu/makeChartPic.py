import matplotlib
matplotlib.use('Agg')
import matplotlib.dates as md
import matplotlib.pyplot as plt
import numpy as np
import datetime as dt
import time
import sqlite3
import sys
import os
import syslog


timestamp=0
value=1

dataLength=0
imgFolder = "chrtsPic"
dbAddress = 'tank.db'

if not (os.path.isdir(imgFolder)):
    os.mkdir(imgFolder)


class Plotting:
    def __init__(self,days=1):
        self.days = days
        self.imageName = "%d.png" % (time.time())
        self.fileAddress = os.path.join( os.path.join(os.getcwd(),imgFolder),self.imageName)
        self.dbData = self.readDB(dbAddress,self.days)
        if((self.dbData == 0) and (len(self.dbData)<1)):
            syslog.syslog(LOG_ERR, "No Records on DB to make the chart")
            exit(0)
        self.TimeStampsList, self.ValueList = self.createTimeValueList(self.dbData)

    def getMaxValue(self):
        return max(self.ValueList)
    
    def getAveValue(self):
        return sum(self.ValueList)/len(self.ValueList)

    def getMinxValue(self):
        return min(self.ValueList)

    @staticmethod
    def createTimeValueList(dbList):
        TimeStampList=[]
        ValueList=[]
        
        for entry in dbList:
            TimeStampList.append(entry[timestamp])
            ValueList.append(entry[value])
        return TimeStampList,ValueList


    def makePlot(self):
        self.TimeStampsList
        self.ValueList
        lastIndexofTimeStamp=len(self.TimeStampsList)-1
        lastIndexofValueList=len(self.ValueList)-1
        now=time.mktime(time.localtime())
        dates=[dt.datetime.fromtimestamp(ts) for ts in self.TimeStampsList]
        #print(dates)
        try:
            datenums=md.date2num(dates)
            plt.xticks(rotation=45)
            ax=plt.gca()
            # ax = plt.axes()
            ax.grid()
            fig = plt.gcf()
            plt.xticks(np.arange(min(datenums), max(datenums)+1, 0.1))
            fig.set_size_inches(17.5, 9.5)
            xfmt = md.DateFormatter('%m-%d %H:%M')
            ax.xaxis.set_major_formatter(xfmt)
            plt.plot(datenums,self.ValueList,marker="8")
            plt.subplots_adjust(bottom=0.3)
            plt.title("Tank Level with {} Reocrds".format(len(self.ValueList)))
            plt.ylabel("Level (cm)")
            plt.savefig(self.fileAddress,dpi=100)
        except Exception as e:
            syslog.syslog(LOG_ERR, "Faild to build chart: %s" %(e))
            exit(0)

    @staticmethod
    def readDB(dbAddress,days):
        try:
            daySecond=86400
            userInput=days
            howManyDays=daySecond*userInput
            now=int(time.time())
            connection = sqlite3.connect(dbAddress)
            cursor= connection.cursor()
            sql = "select epoch, value from level where epoch > %d" % (now-howManyDays)
            # print(sql)
            cursor.execute(sql)
            flowList=cursor.fetchall()
            # dataLength=len(flowList)
            cursor.close()
            return flowList
            
        except Exception as e:
            # print(e)
            return 0
            
    