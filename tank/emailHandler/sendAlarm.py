import smtplib
import sqlite3
import base64
import syslog


dbaddres= "/home/pi/dataBase/tank.db"
_name=0
_email=1
sql = "select * from emailList"
smtpPath = "smtp.gmail.com"
port = 587
devNamePath = "/home/pi/shfiles/FName"
userPassPath = "/home/pi/shfiles/tank/emailHandler/userPass"


class EmailSending:
    def __init__(self,subject,body):
            self.subject = subject
            self.body = body
            self.smtp = smtpPath
            self.port = port
            self.user,self.passwd = self.getUsrPass(userPassPath)
            self.emailList = self.getEmail(dbaddres)
            self.FROM = self.getDevName(devNamePath)
            self.TO = [ent[_email] for ent in self.emailList]
            self.msg = """From: %s\nTo: %s\nSubject: %s\n\n%s""" % (self.FROM,",".join(self.TO),subject,body)
            
            
    @staticmethod
    def getEmail(dbAdress):
        conn = sqlite3.connect(dbAdress)
        return conn.execute(sql).fetchall()

    @staticmethod
    def getDevName(devNamePath):
        with open(devNamePath,'r') as fd:
            return fd.readline()
    
    @staticmethod
    def getUsrPass(userPassPath):
        fd = open(userPassPath,'r')
        filecont = fd.readline()
        fd.close()
        return ([base64.b64decode(content).decode() for content in filecont.split(" ")])

    def sendEmail(self):
        try:
            server  = smtplib.SMTP(self.smtp, self.port)
            server.ehlo()
            server.starttls()
            server.login(self.user,self.passwd)
            server.sendmail(self.FROM,self.TO,self.msg)
            server.close()
        except Exception as e :
            syslog.syslog(syslog.LOG_ERR,"Tank - Email Sending Error with Subject: %s, Error: %s"\
             % (self.subject,e))
            # print("Email error: %s"% (e))



class emailDebugging(EmailSending):
    def __init__(self,subject,body):
        super().__init__(subject,body)
        self.TO=['hatef.8@gmail.com']
        self.msg = """From: %s\nTo: %s\nSubject: %s\n\n%s""" % (self.FROM,",".join(self.TO),subject,body)


