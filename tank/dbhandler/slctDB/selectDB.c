#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>




// typedef int (*sqlite3_callback)(
//    void*,    /* Data provided in the 4th argument of sqlite3_exec() */
//    int,      /* The number of columns in row */
//    char**,   /* An array of strings representing fields in the row */
//    char**    /* An array of strings representing column names */
// );


static int callback(void *data, int argc, char **argv, char **azColName){
    int i;
    for (i=0;i<argc;i++){
        printf("%s = %s\n",azColName[i],argv[i] ? argv[i]: "NULL");
    }
    printf("\n");
    return 0;
}

int main(int argc, char ** argv){
    sqlite3 *db;
    char *errmsg=0;
    char *dbAddrss = "tank.db";
    int rc;
    char *sqlRqst="SELECT * from emailList";


    rc = sqlite3_open(dbAddrss,&db);

    if(rc){
        fprintf(stderr,"Failed to open database\n");
        return 0;
    }
    else{
        rc = sqlite3_exec(db,sqlRqst,callback,0,&errmsg);
        if(rc != SQLITE_OK){
            fprintf(stderr,"SQL error: %s\n",errmsg);
            sqlite3_free(errmsg);
            return(0);
        }
    
    sqlite3_close(db);
    return 0;

    }

}