from sendAlarm import EmailSending


tempHiadrs = "/home/pi/shfiles/alarms/TempHi"

with open(tempHiadrs) as fd:
    hiTemp = fd.readline()

# print(hiTemp)
body = "Temperature goes higher than %sC" % (hiTemp)
subject = "High Temperature Alarm"
myemail = EmailSending(subject,body)
myemail.sendEmail()
del myemail
