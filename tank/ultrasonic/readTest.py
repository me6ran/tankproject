from serial import Serial

serialDevice = "/dev/ttyUSB0" # default for RaspberryPi
maxtry = 5 # seconds to try for a good reading before quitting

def measure(portName):
    counter = 0
    ser = Serial(portName, 9600, 8, 'N', 1, timeout=1)

    valueCount = 0

    while counter < maxtry:
        if ser.inWaiting():
            bytesToRead = ser.inWaiting()
            valueCount += 1
            if valueCount < 2: # 1st reading may be partial number; throw it out
                continue
            testData = ser.read(bytesToRead)
            if not testData.startswith(b'R'):
                # data received did not start with R
                print("noR")
                continue
            try:
                sensorData = testData.decode('utf-8').lstrip('R')
            except UnicodeDecodeError:
                # data received could not be decoded properly
                continue
            try:
                mm = int(sensorData)
            except ValueError:
                # value is not a number
                continue
            ser.close()
            return(mm)
        counter +=1

    ser.close()
    raise RuntimeError("Expected serial data not received")

if __name__ == '__main__':
    measurement = measure(serialDevice)
    print("distance =",measurement)
