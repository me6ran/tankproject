#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>      //multiple file descriptor
#include <stdlib.h>
#include <unistd.h>
#include <sqlite3.h>
#include <time.h>

void temp_db(float);
const char *dbAdrs = "/home/pi/dataBase/tank.db";

static int callback(void *None, int colNum, char **colRec, char **colName){
    for(int i=0;i<colNum;i++){
        printf("%s\n", colRec[i] ? colRec[i]: "NULL");
    }
    printf("\n");
    return 0;
}


int main(void){
    DIR *dir;
    struct dirent *myDirEnt;
    char dev[16]; //device ID
    char devPath[128]; //path to device
    char buf[256]; //data from the device
    char tmpData[6]; //temp c
    char path[]= "/sys/bus/w1/devices";
    ssize_t numRead;
    float temp;
    dir = opendir(path);
    if(dir!=NULL){
       while((myDirEnt = readdir(dir))){
           if(strstr(myDirEnt->d_name,"28-")!=NULL){
            //    printf("%d\n",*(strstr(myDirEnt->d_name,"28-")+2));
               strcpy(dev,myDirEnt->d_name);
            //    printf("device found: %s\n",dev);

           }   
       }
        (void) closedir(dir);
    }
    else{
        perror("Not able to open device directory");
        return 1;
    }

    // create absolute path
    sprintf(devPath, "%s/%s/w1_slave", path, dev);

    int fd = open(devPath, O_RDONLY);
    if(fd==-1){
        perror("Could not open dev file.");
        return -1;
    }

    while((numRead=read(fd,buf,256))>0){

        strncpy(tmpData,strstr(buf,"t=")+2,5);
        temp = strtof(tmpData,NULL);
        
        // example for strtof: 
        // char array[] = "365.25 7.0"; 
    
        // // Character end pointer 
        // char* pend; 
    
        // // f1 variable to store float value 
        // float f1 = strtof(array, &pend); 
    
        // // f2 varible to store float value 
        // float f2 = strtof(pend, NULL); 
  
    }
    close(fd);
    temp_db(temp);
    
    return 0;
}

void temp_db(float mytemp){

    char *errmsg;
    int ret;
    sqlite3 *db;
    char sql[200];
    
    //opening the database
    ret = sqlite3_open(dbAdrs,&db);

    if(ret){
        fprintf(stderr,"Faile to open: %s ",sqlite3_errmsg(db));
        exit(0);
    }
    else{
        time_t epch;
        struct tm *local;
        epch= time(NULL);
        local = localtime(&epch);
        char buffTime[100];
        strftime(buffTime,100,"%b-%d-%g %H:%M",local);
        sprintf(sql,"INSERT INTO temp (epoch,datetime,value) VALUES (%ld,'%s',%.1f)"\
        ,epch,buffTime,mytemp/1000);
        ret = sqlite3_exec(db,sql,callback,0,&errmsg);
        if(ret != SQLITE_OK){
            fprintf(stderr,"SQL error: %s\n",errmsg);
            sqlite3_free(errmsg);
            sqlite3_close(db); 
            exit(0);
        }

        sqlite3_close(db); 

    }

}